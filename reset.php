 <?php

$message = "";

if (isset($_POST['username']) && isset($_POST['current']) && isset($_POST['new']) && isset($_POST['retype']))
{
    if ($_POST['new'] !== $_POST['retype'])
    {
        $message = "Your passwords don't match!";
    }
    elseif ($_POST['current'] === $_PSOT['new'])
    {
        $message = "Your new password must be different.";
    }
    else
    {
        $link = mysql_connect("localhost", "root", "awwhole1");
        mysql_select_db("test", $link) or die("Cannot connect to database.");
        
        $user = mysql_real_escape_string($_POST['username']);
        $pass = mysql_real_escape_string($_POST['new']);
        $curr = mysql_real_escape_string($_POST['current']);
        
        $sql = "UPDATE `test` SET `password` = '{$pass}' WHERE `username` = '{$user}' AND `password` = '{$curr}' LIMIT 1;";
        mysql_query($sql);
        
        if (mysql_affected_rows() == 0)
            $message = "Your username or password was incorrect.";
        else
            $message = "Your password has been successfully updated!";
        
        mysql_close($link);
    }
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Change Password</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        
        <style type="text/css">
            html, body
            {
                padding: 0px;
                margin: 0px;
            }
            
            #passform
            {
                width: 500px;
                font-family: "Courier New", monospace;
                
                margin: 30px auto;
            }
            
            #passform .label
            {
                float: left;
                width: 200px;
            }
            
            #passform .text
            {
                float: right;
                width: 275px;
            }
            
            #passform br
            {
                clear: both;
            }
            
            #passform .submit
            {
                float: right;
            }
            
            #passform .message
            {
                font-size: 12px;
                color: #009;
            }
        </style>
    </head>
    
    <body>
        <form method="post" action="" id="passform">
            <div class="label">Username</div>
            <input type="text" class="text" name="username" />
            
            <br />
            <br />
            
            <div class="label">Current Password</div>
            <input type="password" class="text" name="current" />
            
            <br />
            <br />
            
            <div class="label">New Password</div>
            <input type="password" class="text" name="new" />
            
            <br />
            <br />
            
            <div class="label">Retype Password</div>
            <input type="password" class="text" name="retype" />
            
            <br />
            <br />
            
            <span class="message"><?php echo $message; ?></span>
            <input type="submit" value="Update" class="submit" />
        </form>
    </body>
</html>
