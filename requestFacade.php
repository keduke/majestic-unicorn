<?php
require_once('PackageSubject.php');
require_once('db.php');

class requestFacade implements PackageSubject //apply observer pattern to be the subject to notify the observer
{
	public $user;
	public $request;
	public $package;
	public $service;
	public $members;
	public $subject;

	function __construct($user, $request, $package, $service)
	{
		$this->user = $user;
		$this->request = $request;
		$this->package = $package;
		$this->service = $service;
	}
	function addPackage()
	{
		if(empty($this->user))
		{
			$db::query('add', $package);
			return;
		}
		$this->members = db::getMembers($this->package); //get observers of 
		$db::query('add', $this->package, $this->user);
		$user->notify($this->subject);
	}
	function removePackage()
	{
		if(empty($this->user))
		{
			$db::query('remove', $package);
			return;
		}
		$db::query('remove', $this->package, $this->user);
		$user->notify($this->subject);
	}
	function addService()
	{
		if(empty($this->user))
		{
			$db::query('add', $this->service);
			return;
		}
		$db::query('add', $this->service, $this->user);
		$user->notify($this->subject);
	}
	function addService()
	{
		if(empty($user))
		{
			$db::query('remove', $this->service);
			return;
		}
		$db::query('remove', $this->service, $this->user);
		$user->notify($this->subject);
	}
}
?>