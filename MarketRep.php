<?php
require_once('Member.php');
class MarketRep extends Member
{
	function __construct($name, $street, $city, $state, $country, $email)
	{
		parent::__construct($name, $street, $city, $state, $country, $email);
		$this->setType("MarketRep");
	}
	function addPackage($package)
	{
		$requestFacade = new requestFacade('package', $package);
		$requestFacade->addPackage();
	}
	function removePackage($package)
	{
		$requestFacade = new requestFacade('package', $package);
		$requestFacade->removePackage();
	}
	function addService($service, $package)
	{
		$requestFacade = new requestFacade('service', $service, $package);
		$requestFacade->addPackage();
	}
	function removeService($service, $package)
	{
		$requestFacade = new requestFacade('service', $service, $package);
		$requestFacade->removePackage();
	}

}

?>