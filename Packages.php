<?php
require_once("Products.php");

class Packages implements Products
{
	private $description;
	private $cost;
	private $services;
	private $discount;

	function __construct($description, $discount, $cost)
	{
		$this->description = $description;
		$this->discount = $discount;
		$this->cost = $cost;
	}

	public function addService($service)
	{
		$this->cost += $service->cost;
		$this->description[] $service->description;
		$this->services[] = $service;
	}

	public function getDescriptionAsString()
	{
		$description = $this->description;
		$a = array_pop($description);
		return implode(", ", $description)." ".$a;
	}

	public function getDescription()
	{
		return $this->$description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}
	public function setCost($cost)
	{
		$this->cost = $cost;
	}
	public function getCost()
	{
		if(empty($this->discount))
			$this->discount = 1;
		return $this->cost * $this->discount;
	}
	


}


?>