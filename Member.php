<?php

abstract class Member
{
	private $name;
	private $address = array(
		"street" => "",
		"city" => "",
		"state" => "",
		"country" => "",
		"email" => ""
	);
	private $type;
	function __construct($name, $street, $city, $state, $country, $email)
	{
		$this->name = $name;
		$this->address["street"] = $street;
		$this->address["city"] = $city;
		$this->address["state"] = $state;
		$this->address["country"] = $country;
		$this->address["email"] = $email;
	}
	function login($name, $password)
	{
		return $db->query_login($name, $password);
	}
	public function setType($type)
	{
		$this->$type = $type;
	}
	public function getType($type)
	{
		return $this->type;
	}
	public function setName($name)
	{
		$this->name = $name;
	}
	public function getName()
	{
		return $this->name;
	}
	public function setStreet($street)
	{
		$this->address["street"] = $street;
	}
	public function getStreet()
	{
		return $this->address["street"];
	}
	public function setCity($city)
	{
		$this->address["city"] = $city;
	}
	public function getCity()
	{
		return $this->address["city"];
	}
	public function getState()
	{
		return $this->address["state"];
	}
	public function setState($state)
	{
		$this->address["state"] = $state;
	}
	public function getCountry()
	{
		return $this->address["country"];
	}
	public function setCountry($country)
	{
		$this->address["country"] = $county;
	}
	public function setEmail($email)
	{
		$this->address["email"] = $email;
	}
	public function getEmail()
	{
		return $this->address["email"];
	}
}


?>