<?php
require_once('Member.php')

class User implements Member
{
	private $package;
	private $services;
	private $threshold;
	function __construct($name, $street, $city, $state, $country, $email)
	{
		parent::__construct($name, $street, $city, $state, $country, $email);
		$this->setType("User");
	}
	public function addServices($service)
	{
		$this->services[] = $service;
	}
	public function getService()
	{
		return implode(" ", $services);
	}
	public function setPackage($package)
	{
		$this->package = $package;
	}
	public function getService($package)
	{
		return $this->package;
	}
	public function setThreshHold($int)
	{
		$this->threshold = $int;
	}
	public function notify($subject) //subject is observer
	{
		$package = $subject->getPackage();
		$services = $subject->getService();
		
	}

}


?>