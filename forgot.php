 <?php

$message = "";

if(isset($_POST['username']) && isset($_POST['email']))
{
    $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 8)), 0, 8);
    mail($_POST['email'], "Your password reset", "Hello your password has been temporarily changed to $s  Make sure to change this as soon as possible.");

    $link = mysql_connect("localhost", "root", "awwhole1");
    mysql_select_db("test", $link) or die("Cannot connect to database.");

    $user = mysql_real_escape_string($_POST['username']);

    $sql = "UPDATE `test` SET `password` = '{$s}' WHERE `username` = '{$user}' LIMIT 1;";
    mysql_query($sql);
    $message = "Email sent successfully.";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Forgot Password</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        
        <style type="text/css">
            html, body
            {
                padding: 0px;
                margin: 0px;
            }
            
            #passform
            {
                width: 500px;
                font-family: "Courier New", monospace;
                
                margin: 30px auto;
            }
            
            #passform .label
            {
                float: left;
                width: 200px;
            }
            
            #passform .text
            {
                float: right;
                width: 275px;
            }
            
            #passform br
            {
                clear: both;
            }
            
            #passform .submit
            {
                float: right;
            }
            
            #passform .message
            {
                font-size: 12px;
                color: #009;
            }
        </style>
    </head>
    
    <body>
        <form method="post" action="" id="passform">
            <div class="label">Username</div>
            <input type="text" class="text" name="username" />
            
            <br />
            <br />
            
            <div class="label">Email</div>
            <input type="text" class="text" name="email" />
            
            <br />
            <br />
            
            <span class="message"><?php echo $message; ?></span>
            <input type="submit" value="Submit" class="submit" />
        </form>
    </body>
</html>
