<?php
require_once("Member.php");
class CustomerRep extends Member
{

	function __construct($name, $street, $city, $state, $country, $email)
	{
		parent::__construct($name, $street, $city, $state, $country, $email);
		$this->setType("CustomerRep");
	}
	function addPackage($user, $package)
	{
		$requestFacade = new requestFacade('package', $package, $user);
		$requestFacade->addPackage();
	}
	function removePackage($user, $package)
	{
		$requestFacade = new requestFacade('package', $package, $user);
		$requestFacade->removePackage();
	}
	function addService($user, $service)
	{
		$requestFacade = new requestFacade('service', $service, $user);
		$requestFacade->addPackage();
	}
	function removeService($service, $package)
	{
		$requestFacade = new requestFacade('service', $service, $user);
		$requestFacade->removePackage();
	}
	function deleteUser($user)
	{
		//delete user
	}

}
?>