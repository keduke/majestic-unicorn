<?
interface Subject 
{
	public function notifyObservers();
	public function removeObserver();
	public function registerObserver();
}

?>