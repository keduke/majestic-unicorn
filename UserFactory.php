<?php
require_once('MemberFactory.php');

class UserFactory implements MemberFactory
{
	function create($name, $street, $city, $state, $country, $email)
	{
		return new User($name, $street, $city, $state, $country, $email);
	}
}

?>